<div class="shipping-wrap">
    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="shipping">
                    <p><span>FREE SHIPPING </span> Offered by MAXSHOP - lorem ipsum dolor sit amet mauris accumsan vitate odio tellus</p>
                    <a href="#" class="button">Learn more</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="footer-wrap">
    <div class="container">
        <div class="row">

            <div class="footer clearfix">

                <div class="span3">
                    <div class="widget">
                        <h3>Customer Service</h3>
                        <ul>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Delivery Information</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Terms & Conditions</a></li>
                        </ul>
                    </div>
                </div>

                <div class="span3">
                    <div class="widget">
                        <h3>Information</h3>
                        <ul>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Delivery Information</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Terms & Conditions</a></li>
                        </ul>
                    </div>
                </div>

                <div class="span3">
                    <div class="widget">
                        <h3>My Account</h3>
                        <ul>
                            <li><a href="#">My Account</a></li>
                            <li><a href="#">Order History</a></li>
                            <li><a href="#">Wish List</a></li>
                            <li><a href="#">Newsletter</a></li>
                        </ul>
                    </div>
                </div>

                <div class="span3">
                    <div class="widget">
                        <h3>Contact us</h3>
                        <ul>
                            <li>support@maxshop.com</li>
                            <li>+38649 123 456 789 00</li>
                            <li>Lorem ipsum address street no 24 b41</li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <footer class="clearfix">
                <div class="span5">
                    <p>© 2013 Maxshop Design, All Rights Reserved</p>
                </div>
                <div class="span2 back-top">
                    <a href="#"> <img src="{!! get_template_directory_uri() !!}/dist/css/images/back.png" alt=""></a>
                </div>
                <div class="span5">
                    <div class="social-icon">
                        <a class="rss" href=""></a>
                        <a class="twet" href=""></a>
                        <a class="fb" href=""></a>
                        <a class="google" href=""></a>
                        <a class="pin" href=""> </a>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>
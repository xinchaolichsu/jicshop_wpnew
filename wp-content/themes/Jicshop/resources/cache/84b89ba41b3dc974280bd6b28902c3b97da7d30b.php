<div class="header-bar">
    <div class="container">
        <div class="row">
            <div class="pric-icon span2">
                <a href="#" class="active">&#x20ac;</a>
                <a href="#">&#xa3;</a>
                <a href="#">&#36;</a>
            </div>

            <div class="span10 right">
                <div class="social-strip">
                    <ul>
                        <li><a href="#" class="account">My Account</a></li>
                        <li><a href="#" class="wish">Wish List</a></li>
                        <li><a href="#" class="check">Checkout</a></li>
                    </ul>
                </div>

                <div class="languages">
                    <a href="#" class="english active"><img src="<?php echo get_template_directory_uri(); ?>/dist/css/images/english.png" alt=""></a>
                    <a href="#" class="german"><img src="<?php echo get_template_directory_uri(); ?>/dist/css/images/german.png" alt=""></a>
                    <a href="#" class="japan"><img src="<?php echo get_template_directory_uri(); ?>/dist/css/images/japan.png" alt=""></a>
                    <a href="#" class="turkish"><img src="<?php echo get_template_directory_uri(); ?>/dist/css/images/turkish.png" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="header-top">
    <div class="container">
        <div class="row">

            <div class="span5">
                <div class="logo">
                    <a href="index.html"><img src="<?php echo get_template_directory_uri(); ?>/dist/css/images/logo.png" alt=""></a>
                    <h1><a href="index.html">Ultra Customizable<span> Ecommerce </span>Theme</a></h1>
                </div>
            </div>

            <div class="span5">
                <form>
                    <input type="text" placeholder="Type and hit enter">
                    <input type="submit" value="">
                </form>
            </div>

            <div class="span2">
                <div class="cart">
                    <ul>
                        <li class="first"><a href="#"></a><span></span></li>
                        <li>0 item(s) - $0.00</li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>

<header>
    <div class="container">
        <div class="row">
            <div class="span12">
                <nav class="desktop-nav">
                    <ul class="clearfix">
                        <li>
                            <a href="#">BAGS</a>
                            <ul class="clearfix sub-menu">
                                <li class="clearfix">
                                    <div class="links">
                                       <h3>Furnitures</h3>
                                        <p>
                                            <a href="#">Chair Designs</a>
                                            <a href="#">Sofa Styles</a>
                                            <a href="#">Bedrooms</a>
                                            <a href="#">Table Designs</a>
                                            <a href="#">Another Category</a>
                                            <a href="#">Accesories</a>
                                            <a href="#">Furniture Parts</a>
                                        </p>

                                        <p>
                                            <a href="#">Chair Designs</a>
                                            <a href="#">Sofa Styles</a>
                                            <a href="#">Bedrooms</a>
                                            <a href="#">Table Designs</a>
                                            <a href="#">Another Category</a>
                                            <a href="#">Accesories</a>
                                            <a href="#">Furniture Parts</a>
                                        </p>

                                    </div>
                                    <figure>
                                        <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/dist/css/images/product7.png" alt=""/></a>
                                    </figure>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Jackets</a>
                            <ul class="clearfix sub-menu menu-two">
                                <li class="clearfix">
                                    <div class="links">
                                        <h3>Furnitures</h3>
                                        <p>
                                            <a href="#">Chair Designs</a>
                                            <a href="#">Sofa Styles</a>
                                            <a href="#">Bedrooms</a>
                                            <a href="#">Table Designs</a>
                                            <a href="#">Another Category</a>
                                            <a href="#">Accesories</a>
                                            <a href="#">Furniture Parts</a>
                                        </p>

                                        <p>
                                            <a href="#">Chair Designs</a>
                                            <a href="#">Sofa Styles</a>
                                            <a href="#">Bedrooms</a>
                                            <a href="#">Table Designs</a>
                                            <a href="#">Another Category</a>
                                            <a href="#">Accesories</a>
                                            <a href="#">Furniture Parts</a>
                                        </p>

                                    </div>
                                    <figure>
                                        <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/dist/css/images/product7.png" alt=""/></a>
                                    </figure>
                                </li>
                            </ul>

                        </li>
                        <li>
                            <a href="#">ACCESORIES </a>
                            <ul class="clearfix sub-menu menu-three">
                                <li class="clearfix">

                                    <figure>
                                        <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/dist/css/images/menu-img.png" alt=""/></a>
                                    </figure>
                                    <div class="links">
                                        <h3>Furnitures</h3>
                                        <p>
                                            <a href="#">Chair Designs</a>
                                            <a href="#">Sofa Styles</a>
                                            <a href="#">Bedrooms</a>
                                            <a href="#">Table Designs</a>
                                            <a href="#">Another Category</a>
                                            <a href="#">Accesories</a>
                                            <a href="#">Furniture Parts</a>
                                        </p>

                                        <p>
                                            <a href="#">Chair Designs</a>
                                            <a href="#">Sofa Styles</a>
                                            <a href="#">Bedrooms</a>
                                            <a href="#">Table Designs</a>
                                            <a href="#">Another Category</a>
                                            <a href="#">Accesories</a>
                                            <a href="#">Furniture Parts</a>
                                        </p>

                                        <p>
                                            <a href="#">Chair Designs</a>
                                            <a href="#">Sofa Styles</a>
                                            <a href="#">Bedrooms</a>
                                            <a href="#">Table Designs</a>
                                            <a href="#">Another Category</a>
                                            <a href="#">Accesories</a>
                                            <a href="#">Furniture Parts</a>
                                        </p>

                                        <p>
                                            <a href="#">Chair Designs</a>
                                            <a href="#">Sofa Styles</a>
                                            <a href="#">Bedrooms</a>
                                            <a href="#">Table Designs</a>
                                            <a href="#">Another Category</a>
                                            <a href="#">Accesories</a>
                                            <a href="#">Furniture Parts</a>
                                        </p>

                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">CATEGORIES</a>
                            <ul class="clearfix sub-menu menu-four">
                                <li class="clearfix">

                                    <div class="our-product">
                                        <h3>Chairs</h3>

                                        <div class="clearfix">
                                            <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/dist/css/images/shopping-img.png" alt=""/></a>
                                            <h4>Brown Wood Chair</h4>
                                            <span>$244.00</span>
                                        </div>

                                        <div class="clearfix">
                                            <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/dist/css/images/shopping-img3.png" alt=""/></a>
                                            <h4>Brown Wood Chair</h4>
                                            <span>$244.00</span>
                                        </div>

                                        <div class="clearfix">
                                            <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/dist/css/images/shopping-img5.png" alt=""/></a>
                                            <h4>Brown Wood Chair</h4>
                                            <span>$244.00</span>
                                        </div>

                                    </div>

                                    <div class="our-product">
                                        <h3>Bedrooms</h3>

                                        <div class="clearfix">
                                            <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/dist/css/images/shopping-img.png" alt=""/></a>
                                            <h4>Brown Wood Chair</h4>
                                            <span>$244.00</span>
                                        </div>

                                        <div class="clearfix">
                                            <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/dist/css/images/shopping-img3.png" alt=""/></a>
                                            <h4>Brown Wood Chair</h4>
                                            <span>$244.00</span>
                                        </div>

                                        <div class="clearfix">
                                            <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/dist/css/images/shopping-img5.png" alt=""/></a>
                                            <h4>Brown Wood Chair</h4>
                                            <span>$244.00</span>
                                        </div>

                                    </div>

                                    <div class="our-product">
                                        <h3>SofaS</h3>

                                        <div class="clearfix">
                                            <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/dist/css/images/shopping-img.png" alt=""/></a>
                                            <h4>Brown Wood Chair</h4>
                                            <span>$244.00</span>
                                        </div>

                                        <div class="clearfix">
                                            <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/dist/css/images/shopping-img3.png" alt=""/></a>
                                            <h4>Brown Wood Chair</h4>
                                            <span>$244.00</span>
                                        </div>

                                        <div class="clearfix">
                                            <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/dist/css/images/shopping-img5.png" alt=""/></a>
                                            <h4>Brown Wood Chair</h4>
                                            <span>$244.00</span>
                                        </div>

                                    </div>

                                </li>
                            </ul>
                        </li>
                        <li><a href="#">Manufacters</a></li>
                        <li><a href="#">Sale </a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">Dresses</a></li>
                        <li><a href="#">Jewelry</a></li>
                        <li><a href="#">ShOES</a></li>
                        <li><a href="#">Shirts</a></li>
                        <li>
                            <a href="#">Pages</a>
                            <ul class="clearfix">
                                <li><a href="index.html">Home</a></li>
                                <li><a href="product-grid.html">Product Grid</a></li>
                                <li><a href="product-list.html">Product List</a></li>
                                <li><a href="shopping-cart.html">Shopping cart</a></li>
                                <li><a href="checkout.html">Checkout</a></li>
                                <li><a href="single-product.html">Single Product</a></li>
                                <li><a href="blog.html">Blog</a></li>
                                <li><a href="single.html">Single</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>

                <select>
                    <option>BAGS</option>
                    <option>Jackets</option>
                    <option>ACCESORIES </option>
                    <option>CATEGORIES</option>
                    <option>Manufacters</option>
                    <option>Sale </option>
                    <option>Blog</option>
                    <option>Dresses</option>
                    <option>Jewelry</option>
                    <option>ShOES</option>
                    <option>Shirts</option>
                </select>
            </div>
        </div>
    </div>
</header>
var gulp = require('gulp');
var jshint = require('gulp-jshint');
var fixmyjs = require("gulp-fixmyjs");
module.exports = function() {
    return gulp.src('./assets/js/**/*.js')
        .pipe(fixmyjs(jshint()))
        .pipe(jshint.reporter('default'));
};

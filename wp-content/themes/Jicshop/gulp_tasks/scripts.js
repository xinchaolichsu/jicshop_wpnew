var gulp = require('gulp');
var concat = require('gulp-concat');
module.exports = function() {
    return gulp.src([
            './bower_components/jquery/dist/jquery.js',            
            './bower_components/jquery-ui/jquery-ui.js',
            './bower_components/bootstrap/dist/js/bootstrap.js',
            './assets/js/root_js/jquery.cycle.all.js',
            './assets/js/root_js/modernizr.custom.17475.js',
            './assets/js/root_js/jquery.elastislide.js',
            './assets/js/root_js/jquery.carouFredSel-6.0.4-packed.js',
            './assets/js/root_js/jquery.selectBox.js',
            './assets/js/root_js/jquery.tooltipster.min.js',
            './assets/js/root_js/jquery.prettyPhoto.js',
            './assets/js/root_js/custom.js',
            './assets/js/scripts/*.js',
        ])
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest('./dist/js/'));
};

var gulp = require('gulp');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var concat = require("gulp-concat");

module.exports = [
    ['sass'],
    function() {
        gulp.src([
                // './bower_components/bootstrap/dist/css/bootstrap.css',
                './assets/css/root_css/jquery-ui.min.css',
                './assets/css/root_css/tooltipster.css',
                './assets/css/root_css/ie.css',
                './assets/css/root_css/bootstrap.css',
                './assets/css/root_css/responsive.css',
                './assets/css/root_css/prettyPhoto.css',
                './assets/css/root_css/style.css',
                './dist/css/app.css'
            ])
            .pipe(cssmin())
            .pipe(rename({
                suffix: '.min'
            }))
            .pipe(concat("app.min.css"))
            .pipe(gulp.dest('./dist/css/'));
    }
];
